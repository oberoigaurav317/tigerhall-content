## Getting Started

First, install the node dependencies:

```bash
npm install
# or
yarn install
```

Setup the env file. Create a ".env" file in the root of the project and populate as per needed.

```bash
cp .env.example .env
```

Then, run the development server:

```bash
npm run start
# or
yarn
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
