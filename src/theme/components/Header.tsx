import { defineStyleConfig } from '@chakra-ui/react';

const Heading = defineStyleConfig({
  baseStyle: {
    fontSize: '2xl', // Default font size
  },
  sizes: {
    xs: {
      fontSize: '12px',
      fontWeight: 500,
      lineHeight: '14px',
      letterSpacing: '0.015em',
    },
    md: {
      fontSize: '16px',
      fontWeight: 700,
      lineHeight: '19px',
      letterSpacing: '0.015em',
    },
    '2xl': {
      fontSize: '24px',
    },
    '3xl': {
      fontSize: '28px',
    },
    '4xl': {
      fontSize: '32px',
    },
    '5xl': {
      fontSize: '36px',
    },
    '6xl': {
      fontSize: '40px',
    },
    '7xl': {
      fontSize: '52px',
    },
    '8xl': {
      fontSize: '64px',
    },
    '9xl': {
      fontSize: '80px',
    },
    '10xl': {
      fontSize: '108px',
    },
  },
});

export default Heading;
