export interface ContentCardsFilter {
  filter: {
    limit: number;
    keywords: string;
    types: Array<
      | 'EBOOK'
      | 'PODCAST'
      | 'EVENT'
      | 'EXPERT'
      | 'STREAM'
      | 'LEARNING_PATH'
      | 'CATEGORY'
      | 'LIFE_GOAL'
    >;
  };
}

export interface ContentCards {
  image: {
    uri: string;
  };
  name: string;
  categories: Array<{
    name: string;
  }>;
  experts: Array<{
    firstName: string;
    lastName: string;
    title: string;
    company: string;
  }>;
  length: number;
  timeSpentOnByUsers: number;
}
