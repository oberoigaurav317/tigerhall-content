/**
 * @jest-environment jsdom
 */
import React from 'react';
import './test-helper';
import {
  fireEvent,
  render,
  waitFor,
} from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import { App, defaultProp } from '../App';
import { CONTENT_CARDS } from '../apollo/queries/contentCards';
import mockData from '../mocks/content.json';

// Mock the data that Apollo Client would return
const mocks = [
  {
    delay: 30,
    request: {
      query: CONTENT_CARDS,
      variables: { ...defaultProp },
    },
    result: {
      data: {
        contentCards: {
          edges: mockData,
        },
      },
    },
  },
];

describe('App Component', () => {
  beforeEach(() => {
    jest.spyOn(console, 'warn').mockImplementation(() => {});
  });

  test('renders App component', async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <App />
      </MockedProvider>
    );
  });

  test('performs search on input change', async () => {
    const { getByRole, getByTestId } = render(
      <MockedProvider mocks={mocks}>
        <App />
      </MockedProvider>
    );

    const searchInput = getByRole('textbox');
    // Type in the search input
    fireEvent.change(searchInput, { target: { value: 'test' } });

    expect(getByTestId('content-results-skeleton')).toBeInTheDocument();
    const mockSpy = mocks[0].result;
    await waitFor(() =>
      expect(mockSpy).toEqual(
        expect.objectContaining({
          data: {
            contentCards: {
              edges: mockData,
            },
          },
        })
      )
    );
    await waitFor(() =>
      expect(getByTestId('content-results-wrapper')).toBeInTheDocument()
    );
  });

  it('should show error UI', async () => {
    const newMock = [
      {
        request: {
          query: CONTENT_CARDS,
          variables: { ...defaultProp },
        },
        error: new Error('An error occurred'),
      },
    ];
    const { getByText } = render(
      <MockedProvider mocks={newMock} addTypename={false}>
        <App />
      </MockedProvider>
    );
    await waitFor(() =>
      expect(
        getByText('Some error occurred while fetching content')
      ).toBeInTheDocument()
    );
  });

  it('check for refetch', async () => {
    let totalCallCount = 0;

    const mock2 = [
      {
        delay: 30,
        request: {
          query: CONTENT_CARDS,
          variables: { ...defaultProp },
        },
        newData: () => {
          if (totalCallCount) {
            return {
              data: {
                contentCards: {
                  edges: mockData.slice(0, 2),
                },
              },
            };
          }
          totalCallCount += 1;
          return {
            data: {
              contentCards: {
                edges: mockData.slice(0, 1),
              },
            },
          };
        },
      },
    ];
    const { getByRole, getAllByTestId } = render(
      <MockedProvider mocks={mock2}>
        <App />
      </MockedProvider>
    );
    await waitFor(() => expect(getAllByTestId('blog-card').length).toEqual(1));

    const searchInput = getByRole('textbox');
    // // Type in the search input
    fireEvent.change(searchInput, { target: { value: 'test' } });

    await waitFor(() => expect(getAllByTestId('blog-card').length).toEqual(2));
  });
});
