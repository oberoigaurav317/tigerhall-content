/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  ApolloClient,
  InMemoryCache,
  ApolloLink,
  HttpLink,
} from '@apollo/client';
import { onError } from '@apollo/client/link/error';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const graphQlWrapper = () => {
  return new ApolloClient({
    ssrMode: false,
    link: ApolloLink.from([
      onError(({ graphQLErrors, networkError }) => {
        if (graphQLErrors) {
          // handle graphql error
          // reload page and let the server handle the request
          if (graphQLErrors[0].message === 'Token error') {
            window.location.reload();
          }
        }
        if (networkError) {
          // handle network error
          console.error(networkError);
        }
      }),
      new HttpLink({
        uri: process.env.REACT_APP_API_BASE_URL,
      }),
    ]),
    ssrForceFetchDelay: 100,
    cache: new InMemoryCache(),
  });
};
export default graphQlWrapper;
