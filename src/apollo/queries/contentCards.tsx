import { gql } from '@apollo/client';

export const CONTENT_CARDS = gql`
  query ContentCardsQuery($filter: ContentCardsFilter) {
    contentCards(filter: $filter) {
      edges {
        ... on Podcast {
          image {
            uri
          }
          name
          categories {
            name
          }
          experts {
            firstName
            lastName
            title
            company
          }
          length
          timeSpentOnByUsers
        }
      }
    }
  }
`;
