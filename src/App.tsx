import React, { useEffect, useMemo, useState } from 'react';
import {
  ChakraProvider,
  Box,
  Grid,
  InputGroup,
  Input,
  InputLeftElement,
  Flex,
  Heading,
} from '@chakra-ui/react';
import { SearchIcon } from '@chakra-ui/icons';
import { useQuery } from '@apollo/client';
import theme from './theme';
import { BlogCard, BlogCardSkeleton } from './components';
import { CONTENT_CARDS } from './apollo/queries/contentCards';
import { ContentCards, ContentCardsFilter } from './types';

export const defaultProp: ContentCardsFilter = {
  filter: {
    limit: 20,
    keywords: '',
    types: ['PODCAST'],
  },
};
export const App = () => {
  const [content, setContent] = useState<ContentCards[]>([]);
  const [searchText, setSearchText] = useState('');
  const {
    loading,
    error,
    data,
    refetch: getContent,
  } = useQuery(CONTENT_CARDS, {
    variables: { ...defaultProp },
  });

  useEffect(() => {
    // Perform Refetching with keyword
    const performSearch = (query: string) => {
      let refetchProps = { ...defaultProp };
      refetchProps.filter.keywords = query;

      // Replace this with your actual search logic
      getContent({ ...refetchProps });
    };

    // Debounce function
    const debounce = (func: (val: string) => void, delay: number) => {
      let timeoutId: NodeJS.Timeout | string | number | undefined;
      return (...args: any) => {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(() => {
          func(args[0]);
        }, delay);
      };
    };

    // Debounced search function
    const debouncedSearch = debounce(performSearch, 300);

    debouncedSearch(searchText);
  }, [getContent, searchText]);

  useEffect(() => {
    // Update state on data update from API
    if (data?.contentCards?.edges?.length) {
      setContent(data?.contentCards?.edges);
    }
  }, [data]);

  // Manage UI for showing skeleton, error or data
  const FlexItems = useMemo(() => {
    if (loading) {
      return new Array(2)
        .fill(undefined)
        .map((_, index) => <BlogCardSkeleton key={`card${index}`} />);
    }
    if (error) {
      return (
        <Heading
          fontFamily='montreal'
          size='xs'
          fontWeight='bold'
          color={'white'}
        >
          Some error occurred while fetching content
        </Heading>
      );
    }

    return content.map((d: ContentCards, index: number) => (
      <BlogCard {...d} key={`card${index}`} />
    ));
  }, [loading, error, content]);

  return (
    <ChakraProvider theme={theme}>
      <Box textAlign='center' fontSize='xl'>
        <Grid p={3}>
          <Box p={12} pt={10}>
            <Flex alignItems={'center'} justifyContent={'center'}>
              <InputGroup maxW={'xl'}>
                <InputLeftElement>
                  <SearchIcon color='grey.400' />
                </InputLeftElement>
                <Input
                  type='text'
                  borderRadius='md'
                  borderColor='grey.700'
                  color='white'
                  borderWidth='1px'
                  py='2'
                  px='4'
                  bgColor='grey.900'
                  value={searchText}
                  onChange={(e) => setSearchText(e.target.value)}
                />
              </InputGroup>
            </Flex>
            {loading ? (
              <Flex
                mt={10}
                alignItems={'center'}
                gap={8}
                wrap={'wrap'}
                justifyContent={'center'}
                data-testid='content-results-skeleton'
              >
                {new Array(2).fill(undefined).map((_, index) => (
                  <BlogCardSkeleton key={`card${index}`} />
                ))}
              </Flex>
            ) : (
              <Flex
                mt={10}
                alignItems={'flex-start'}
                gap={8}
                wrap={'wrap'}
                justifyContent={'center'}
                data-testid='content-results-wrapper'
              >
                {FlexItems}
              </Flex>
            )}
          </Box>
        </Grid>
      </Box>
    </ChakraProvider>
  );
};
