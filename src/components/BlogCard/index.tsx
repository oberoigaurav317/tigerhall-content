import React, { useMemo } from 'react';
import {
  Box,
  Card,
  CardBody,
  chakra,
  Flex,
  Heading,
  Skeleton,
  SkeletonText,
  Stack,
} from '@chakra-ui/react';
import PercentageTag from '../PercentageTag';
import styles from './BlogCard.module.css';
import { ContentCards } from '../../types';

const BlogCard = ({
  image,
  name,
  categories,
  experts,
  length,
  timeSpentOnByUsers,
}: ContentCards) => {
  const imageURL = useMemo(() => {
    let url = new URL(image.uri);
    return `${url.origin}/resize/250x${url.pathname}`;
  }, [image]);

  const expertsName = useMemo(() => {
    return experts.map((e) => (e.firstName + e.lastName).trim()).join(', ');
  }, [experts]);

  const expertsCompany = useMemo(() => {
    const companies = experts.map((e) => e.company.trim()).join(', ');
    if (!companies.length) {
      return 'N.A.';
    }
    return companies;
  }, [experts]);

  return (
    <Card
      maxW='md'
      minW={'md'}
      position='relative'
      bgColor='transparent'
      className={styles.cardWrapper}
      data-testid='blog-card'
    >
      <CardBody bgColor='white' borderRadius='xl' color='black' p={0}>
        <Box position='relative'>
          <chakra.img
            src={imageURL}
            borderTopLeftRadius='xl'
            borderTopRightRadius='xl'
            position='relative'
            objectFit={'cover'}
            width={'100%'}
          />
          <PercentageTag percentage={timeSpentOnByUsers} />
          <Flex className={styles.headphoneWrapper} alignItems={'center'}>
            <chakra.img
              src='/icons/headphones.png'
              className={styles.repIcon}
            />
          </Flex>
          <Flex className={styles.timer} alignItems={'center'} gap='4px'>
            <chakra.img src='/icons/clock.png' className={styles.repIcon} />
            <Heading
              fontFamily='montreal'
              size='xs'
              fontWeight='bold'
              color={'white'}
            >
              {length}m
            </Heading>
          </Flex>
        </Box>
        <Box
          data-testid='card-progressLine'
          className={styles.progressLine}
          width={`${timeSpentOnByUsers > 0 ? timeSpentOnByUsers : 2}%`}
        ></Box>
        <Box className={styles.contentWrapper} p={3} textAlign='left'>
          <Heading
            fontFamily='montreal'
            size='xs'
            color='grey.700'
            mb={1}
            textTransform={'uppercase'}
          >
            {categories.map((c) => c.name).join(', ')}
          </Heading>
          <Heading fontFamily='montreal' size='md' color='black'>
            {name}
          </Heading>

          <Heading
            fontFamily='montreal'
            size='xs'
            color='grey.800'
            mt={2}
            mb={1}
            textTransform={'capitalize'}
          >
            {expertsName}
          </Heading>
          <Heading
            fontFamily='montreal'
            fontWeight='700'
            size='xs'
            color='grey.700'
            textTransform={'capitalize'}
          >
            {expertsCompany}
          </Heading>
          <Flex
            alignItems={'center'}
            gap='10px'
            justifyContent={'flex-end'}
            p={1}
          >
            <chakra.img src='/icons/share.png' className={styles.actionIcon} />
            <chakra.img
              src='/icons/bookmark.png'
              className={styles.actionIcon}
            />
          </Flex>
        </Box>
      </CardBody>
    </Card>
  );
};

export const BlogCardSkeleton = () => {
  return (
    <Card
      maxW='md'
      minW={'md'}
      position='relative'
      bgColor='transparent'
      className={styles.cardWrapper}
    >
      <CardBody bgColor='white' borderRadius='xl' color='black' p={0}>
        <Stack>
          <Skeleton height='150px' />
          <Box className={styles.contentWrapper} p={3} textAlign='left'>
            <Skeleton height='20px' mb={2} />
            <Skeleton height='40px' mb={1} />
            <SkeletonText mt='2' noOfLines={2} skeletonHeight='4' width={40} />
          </Box>
        </Stack>
      </CardBody>
    </Card>
  );
};

export default BlogCard;
