/**
 * @jest-environment jsdom
 */
import React from 'react';
import '../../tests/test-helper';
import { render } from '@testing-library/react';
import BlogCard from './index';
import mockData from '../../mocks/content.json';

describe('BlogCard Component', () => {
  test('renders BlogCard component', async () => {
    const { getByText } = render(<BlogCard {...mockData[0]} />);
    expect(getByText('category April')).toBeInTheDocument();
  });

  test('expert companies to be NA', async () => {
    let mockData2 = { ...mockData[0] };
    mockData2.categories = [];
    const { getByText } = render(<BlogCard {...mockData2} />);
    expect(getByText('N.A.')).toBeInTheDocument();
  });

  test('expert companies be multiple', async () => {
    let mockData2 = { ...mockData[0] };
    mockData2.experts = [
      {
        __typename: 'Expert',
        firstName: 'Test',
        lastName: '',
        title: '',
        company: 'Company1',
      },
      {
        __typename: 'Expert',
        firstName: 'Test2',
        lastName: '',
        title: '',
        company: 'Company2',
      },
    ];
    const { getByText } = render(<BlogCard {...mockData2} />);
    expect(getByText('Company1, Company2')).toBeInTheDocument();
  });

  test('progress line with 0 timeSpentOnByUsers', async () => {
    const { getByTestId } = render(<BlogCard {...mockData[0]} />);
    expect(getByTestId('card-progressLine')).toBeInTheDocument();
    const styles = getComputedStyle(getByTestId('card-progressLine'));
    expect(styles.width).toBe('2%');
  });

  test('progress line with positive timeSpentOnByUsers', async () => {
    let mockData2 = { ...mockData[0] };
    mockData2.timeSpentOnByUsers = 10;
    const { getByTestId } = render(<BlogCard {...mockData2} />);
    const styles = getComputedStyle(getByTestId('card-progressLine'));
    expect(styles.width).toBe('10%');
  });
});
