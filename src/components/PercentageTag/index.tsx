import React from 'react';
import { Box, Flex, Heading } from '@chakra-ui/react';
import styles from './PercentageTag.module.css';

const PercentageTag = ({ percentage }: { percentage: number }) => {
  const percentDegree = Math.ceil(percentage * 3.6);
  const chartStyle = {
    background: `conic-gradient(
          var(--chakra-colors-orange-400) 0deg ${percentDegree}deg,
          var(--chakra-colors-white) ${percentDegree}deg 360deg,
          var(--chakra-colors-white) 360deg 360deg
        )`,
  };

  return (
    <Box bgColor={'white'} className={styles.tagWrapper}>
      <Flex alignItems={'center'} gap='5px'>
        <div style={chartStyle} className={styles.pieChart}></div>
        <Heading
          fontFamily='montreal'
          size='xs'
          fontWeight='bold'
          color='grey.900'
        >
          {percentage}% Completed
        </Heading>
      </Flex>
    </Box>
  );
};

export default PercentageTag;
